//package com.example.paraf_test_project.repository.Pagination
//
//import androidx.paging.PagingSource
//import androidx.paging.PagingState
//import com.example.paraf_test_project.model.VenueResponse
//import com.example.paraf_test_project.services.FoursquareApiService
//import retrofit2.HttpException
//import java.io.IOException
//
//
//private const val STARTING_PAGE_INDEX = 1
//
//
//class VenuePagingSource(
//    private val service: FoursquareApiService
//) : PagingSource<Int, VenueResponse>() {
//
//
//    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, VenueResponse> {
//        val pageIndex = params.key ?: STARTING_PAGE_INDEX
//        return try {
//
////            since the given api is not paginated , it's not possible to page it !!
//
//            val response = service.getVenues(
//                page = pageIndex
//            )
//            val veneus = respones.result
//            val nextKey =
//                if (veneus.isEmpty()) {
//                    null
//                } else {
//
//                    pageIndex + (params.loadSize / NETWORK_PAGE_SIZE)
//                }
//            LoadResult.Page(
//                data = veneus,
//                prevKey = if (pageIndex == STARTING_PAGE_INDEX) null else pageIndex,
//                nextKey = nextKey
//            )
//        } catch (exception: IOException) {
//            return LoadResult.Error(exception)
//        } catch (exception: HttpException) {
//            return LoadResult.Error(exception)
//        }
//    }
//
//    /**
//     * The refresh key is used for subsequent calls to PagingSource.Load after the initial load.
//     */
//    override fun getRefreshKey(state: PagingState<Int, VenueResponse>): Int? {
//
//        return state.anchorPosition?.let { anchorPosition ->
//            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
//                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
//        }
//    }
//}